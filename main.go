package main

import "fmt"


func main() {

	test1:=[]int{1,2,4,1,2,5,8,2}
	sortColors(test1)
	fmt.Println(test1)
}

func sortColors(nums []int)  {
    for i:=0; i< len(nums)-1; i++ {
      for j:=0; j < len(nums)-i-1; j++ {
         if (nums[j] > nums[j+1]) {
            nums[j], nums[j+1] = nums[j+1], nums[j]
         }
      }
   }
}